"use strict";

const NS = "http://www.w3.org/2000/svg";
const alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
let inc = -1;
let standElement = document.getElementById('stand');
let chooseLetters = [];
let stopAnimation = false;

let actualChar = '';

let charSuccess = [];

function success(key) {
    if (charSuccess.indexOf(key) != -1)
        return;
    charSuccess.push(key);
    let contentSVG = document.getElementById('canvas');
    contentSVG.classList.add('shake');
    let span = document.createElement('span');
    span.textContent = key;
    document.getElementById('success').appendChild(span);
    setTimeout(
        function() { 
            contentSVG.classList.remove('shake');
        },
        1000
    );
}

function tryShot(key) {
    if (actualChar === key.toLowerCase()) {
        success(key);
    }
}

function showPress(evt) {
    if (evt.key === 'Enter') {
        zoom(true);
    }
    if (alphabet.toLowerCase().indexOf(evt.key) !== -1) {
        tryShot(evt.key);
    }
    /*console.error(evt.key);
    var stroke = window.getComputedStyle(chooseLetters[10]).stroke;
    console.error(chooseLetters[10]);
    console.error(stroke);
    */
}

function giveLetter(p) {
    return p.charAt(Math.floor(Math.random() * p.length));
}

function zoom(_in) {
    if (_in) {
        document.getElementById('begin').classList.remove('notransition');
        document.getElementById('animation-mask').classList.remove('notransition');
        document.body.classList.add('zoom');
        stopAnimation = true;
    } else {
        document.body.classList.remove('zoom');
    }
}

let timer;
let ctx = null;
let c_x = 0;
let c_y = 0;

let y = 0;
let translate = 0;
let rotate = 0;
let radian_rotate = 0;


function give_visu_letter(char) {
    actualChar = char.toLowerCase();
    document.getElementById('dev-info').innerHTML = char;
}

function _rotate(char, deg, center_x, center_y, _selected) {
    radian_rotate = 0;
    let _x = 100;
    let rec_width = 50;
    ctx.translate(center_x, center_y);
    radian_rotate = (deg - 90) * Math.PI / 180;
    ctx.rotate(radian_rotate);
    
    ctx.fillStyle = "white";
    if (_selected) {
        ctx.strokeStyle = "red";
    } else {
        ctx.strokeStyle = "black";
    }
    if (center_x === 370 && deg === 0) {
        give_visu_letter(char);
    }
    ctx.strokeRect(_x, 0, rec_width, rec_width);
    ctx.fillRect(_x, 0, rec_width, rec_width);
    ctx.stroke();
    
    ctx.rotate(Math.PI / 2);
    ctx.translate(-rec_width, -rec_width - _x);
    ctx.font = "30px Arial";
    ctx.fillStyle = "black";
    ctx.fillText(char, _x + -40, 40);
    ctx.translate(rec_width, rec_width + _x);
    ctx.rotate(-Math.PI / 2);
            
    ctx.rotate(- radian_rotate);
    ctx.translate(-center_x, -center_y);
}

let nb_redraw = 0;
let step = 5;
let rotate_between = 180 / 6;
let space_between = 75;
let rotate_step = rotate_between * step / space_between;
let init_x = 225;
let x = init_x;
let trans_y = 200;
let degre = 0;

function draw() {
    x = init_x;
    nb_redraw++;
    ctx.clearRect(0, 0, c_x, c_y);
    ctx.save();

    var possible = alphabet;
    while (possible.length > 0) {
        let pos = x + nb_redraw * step;
        let letter = possible.charAt(0);
        possible = possible.replace(letter, '');
        inc++;
        let limit_one = init_x + 7 * space_between;
        let limit_two = init_x + 13 * space_between;
        let limit_three = init_x + 20 * space_between;
        let limit_four = init_x + 26 * space_between;
        let limit_five = init_x + 33 * space_between;
        let limit_six = init_x + 39 * space_between;
        let limit_seven = init_x + 46 * space_between;
        let limit_height = init_x + 51 * space_between;
        if ((pos >= limit_one && pos <= limit_two)) {
            let _deg = (inc - 7) * rotate_between + rotate_step * nb_redraw;
            _rotate(letter, _deg, limit_one, trans_y);
            x += space_between;
            continue;
        }
        if (pos > limit_two && pos <= limit_three) {
            let result = limit_one - pos;
            result = limit_one + (16 - pos / space_between) * space_between;
            _rotate(letter, 180, result, trans_y);
            x += space_between;
            continue;
        }
        if (pos >= limit_three && pos <= limit_four) {
            let _deg = 180 + (inc - 20) * rotate_between + rotate_step * nb_redraw;
            _rotate(letter, _deg, init_x, trans_y);
            x += space_between;
            continue;
        }
        if (pos >= limit_four && pos <= limit_five) {
            _rotate(letter, 0, init_x + pos - 29 * space_between, trans_y);
            x += space_between;
            continue;
        }
        if (pos >= limit_five && pos <= limit_six) {
            let _deg = (inc - 33) * rotate_between + rotate_step * nb_redraw;
            _rotate(letter, _deg, limit_one, trans_y);
            x += space_between;
            continue;
        }
        if (pos > limit_six && pos <= limit_seven) {
            let result = limit_one + (42 - pos / space_between) * space_between;
            _rotate(letter, 180, result, trans_y);
            x += space_between;
            continue;
        }
        if (pos > limit_seven && pos <= limit_height) {
            let _deg = 180 + (inc - 46) * rotate_between + rotate_step * nb_redraw;
            _rotate(letter, _deg, init_x, trans_y);
            x += space_between;
            if (pos == limit_height) {
                x = 0;
                nb_redraw = 0;
            }
            continue;
        }
        _rotate(letter, 0, pos, trans_y);
        x += space_between;
    }
    
    ctx.stroke();
    ctx.restore();
    inc = -1;
}

document.addEventListener('DOMContentLoaded', function(){
    var canvas = document.getElementById('animation');
    c_x = canvas.width = 1000;
    c_y = canvas.height = 500;
    canvas.style.display = 'block';
    canvas.style.margin  = ' 0 auto';
    ctx = canvas.getContext('2d');

    draw();
    timer = setInterval(draw, 200);
});

document.onkeypress = showPress;
